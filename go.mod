module github.com/ereyes01/google-for-a-day

go 1.12

require (
	github.com/golang/mock v1.3.1
	github.com/google/go-cmp v0.3.1
	golang.org/x/net v0.0.0-20191105084925-a882066a44e0
)
