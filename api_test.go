package search

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	gomock "github.com/golang/mock/gomock"
	"github.com/google/go-cmp/cmp"
)

func TestClear(t *testing.T) {
	mockCtl := gomock.NewController(t)
	mockIndexer := NewMockindexer(mockCtl)
	api := &SearchAPI{i: mockIndexer}

	mockIndexer.EXPECT().Clear()

	req := httptest.NewRequest("GET", "http://minigoogle.com/clear", nil)
	w := httptest.NewRecorder()
	api.ClearHandler(w, req)

	resp := w.Result()
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		t.Fatalf("response code got: %d expected: %d", resp.StatusCode, http.StatusOK)
	}

	mockCtl.Finish()
}

func TestCrawl(t *testing.T) {
	mockCtl := gomock.NewController(t)
	mockIndexer := NewMockindexer(mockCtl)
	api := &SearchAPI{i: mockIndexer}

	mockIndexer.EXPECT().Crawl("http://some-site.com").Return(50, 5, nil)

	params := url.Values{}
	params.Add("site", "http://some-site.com")

	baseURL, err := url.Parse("http://minigoogle.com/crawl")
	if err != nil {
		t.Fatal("parse base url:", err)
	}

	baseURL.RawQuery = params.Encode()

	req := httptest.NewRequest("GET", baseURL.String(), nil)
	w := httptest.NewRecorder()
	api.CrawlHandler(w, req)

	resp := w.Result()
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		t.Fatalf("response code got: %d expected: %d", resp.StatusCode, http.StatusOK)
	}

	var payload CrawlResponse
	if err := json.NewDecoder(resp.Body).Decode(&payload); err != nil {
		t.Fatal("json decode response:", err)
	}

	expResponse := CrawlResponse{Words: 50, Sites: 5}
	if !cmp.Equal(expResponse, payload) {
		t.Fatal("unexpected crawl response:", cmp.Diff(expResponse, payload))
	}

	mockCtl.Finish()
}

func TestSearch(t *testing.T) {
	mockCtl := gomock.NewController(t)
	mockIndexer := NewMockindexer(mockCtl)
	api := &SearchAPI{i: mockIndexer}

	expResponse := []SearchHit{
		{
			URL:         "http://funnywords.com",
			Title:       "Funny Words",
			Occurrences: 50,
		},
		{
			URL:         "http://seriouswords.com",
			Title:       "Serious Words",
			Occurrences: 10,
		},
	}

	mockIndexer.EXPECT().Search("rutabaga").Return([]searchResult{
		{
			site: &site{
				URL:   "http://funnywords.com",
				Title: "Funny Words",
			},
			Occurrences: 50,
		},
		{
			site: &site{
				URL:   "http://seriouswords.com",
				Title: "Serious Words",
			},
			Occurrences: 10,
		},
	})

	params := url.Values{}
	params.Add("word", "rutabaga")

	baseURL, err := url.Parse("http://minigoogle.com/search")
	if err != nil {
		t.Fatal("parse base url:", err)
	}

	baseURL.RawQuery = params.Encode()

	req := httptest.NewRequest("GET", baseURL.String(), nil)
	w := httptest.NewRecorder()
	api.SearchHandler(w, req)

	resp := w.Result()
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		t.Fatalf("response code got: %d expected: %d", resp.StatusCode, http.StatusOK)
	}

	var payload []SearchHit
	if err := json.NewDecoder(resp.Body).Decode(&payload); err != nil {
		t.Fatal("json decode response:", err)
	}

	if !cmp.Equal(expResponse, payload) {
		t.Fatal("unexpected search response:", cmp.Diff(expResponse, payload))
	}

	mockCtl.Finish()
}
