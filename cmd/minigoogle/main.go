package main

import (
	"flag"
	"log"
	"net/http"

	search "github.com/ereyes01/google-for-a-day"
)

var bind string

func init() {
	flag.StringVar(&bind, "bind", ":9090", "host:port you want to bin the server to")
}

func main() {
	flag.Parse()
	api := search.NewSearchAPI()

	http.HandleFunc("/crawl", api.CrawlHandler)
	http.HandleFunc("/search", api.SearchHandler)
	http.HandleFunc("/clear", api.ClearHandler)

	log.Printf("listening on %s...", bind)
	log.Fatal(http.ListenAndServe(bind, nil))
}
