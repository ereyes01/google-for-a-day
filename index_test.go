package search

import (
	"sync"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestClearIndex(t *testing.T) {
	i := newIndexerMap()
	i.index["word"] = wordMatches{"url-1": 10}
	i.sites["url-1"] = &site{URL: "url-1", Title: "a site"}

	i.Clear()

	if len(i.index) > 0 {
		t.Fatal("index is not cleared / empty")
	}

	if len(i.sites) > 0 {
		t.Fatal("sites is not cleared / empty")
	}
}

func TestIndexWord(t *testing.T) {
	i := newIndexerMap()

	i.AddWord("rutabaga", "funnywords.com")
	i.AddWord("rutabaga", "funnywords.com")
	i.AddWord("mofongo", "funnywords.com")
	i.AddWord("the", "commonwords.com")
	i.AddWord("rutabaga", "commonwords.com")

	expectedIndex := map[string]wordMatches{
		"rutabaga": wordMatches{"funnywords.com": 2, "commonwords.com": 1},
		"mofongo":  wordMatches{"funnywords.com": 1},
		"the":      wordMatches{"commonwords.com": 1},
	}

	if !cmp.Equal(expectedIndex, i.index) {
		t.Fatal("unexpected index:", cmp.Diff(expectedIndex, i.index))
	}
}

func TestAddSite(t *testing.T) {
	i := newIndexerMap()

	i.AddSite("funnywords.com", "Funny Words")
	i.AddSite("commonwords.com", "Common Words")

	expectedSites := map[string]*site{
		"funnywords.com":  &site{URL: "funnywords.com", Title: "Funny Words"},
		"commonwords.com": &site{URL: "commonwords.com", Title: "Common Words"},
	}

	if !cmp.Equal(expectedSites, i.sites) {
		t.Fatal("unexpected sites:", cmp.Diff(expectedSites, i.sites))
	}
}

func TestSearchWord(t *testing.T) {
	i := &indexerMap{
		index: map[string]wordMatches{
			"rutabaga":  wordMatches{"a.com": 100, "b.com": 50, "c.com": 80, "d.com": 1000},
			"loose-end": wordMatches{"c.com": 1},
		},
		sites: map[string]*site{
			"a.com": &site{URL: "a.com", Title: "a"},
			"b.com": &site{URL: "b.com", Title: "b"},
			"d.com": &site{URL: "d.com", Title: "d"},
			// c is missing, shouldn't happen, but I guess it shouldn't be undefined
		},
	}

	results := i.Search("rutabaga")

	// results sorted by # occurrences
	expectedResults := []searchResult{
		{
			site:        &site{URL: "d.com", Title: "d"},
			Occurrences: 1000,
		},
		{
			site:        &site{URL: "a.com", Title: "a"},
			Occurrences: 100,
		},
		{
			site:        &site{URL: "b.com", Title: "b"},
			Occurrences: 50,
		},
		// c is omitted, since it's not in sites
	}

	if !cmp.Equal(expectedResults, results, cmp.AllowUnexported(searchResult{})) {
		t.Fatal("unexpected search results:", cmp.Diff(expectedResults, results, cmp.AllowUnexported(searchResult{})))
	}

	shouldBeEmpty := i.Search("missing")
	if shouldBeEmpty != nil {
		t.Fatal("search for un-indexed word must turn up nil")
	}

	shouldAlsoBeEmpty := i.Search("loose-end") // only in c.com, pathological case
	if shouldAlsoBeEmpty != nil {
		t.Fatal("search for word in unseen site must turn up nil")
	}
}

// need to run with race detector: go test -race
func TestIndexerThreadSafe(t *testing.T) {
	i := newIndexerMap()
	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()
		i.AddSite("funnywords.com", "Funny Words")
		i.AddSite("commonwords.com", "Common Words")
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		i.IsSiteIndexed("funnywords.com")
		i.IsSiteIndexed("funnywords.com")
		i.IsSiteIndexed("funnywords.com")
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		i.AddWord("rutabaga", "funnywords.com")
		i.AddWord("rutabaga", "funnywords.com")
		i.AddWord("mofongo", "funnywords.com")
		i.AddWord("the", "commonwords.com")
		i.AddWord("rutabaga", "commonwords.com")
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		i.AddWord("rutabaga", "funnywords.com")
		i.AddWord("rutabaga", "funnywords.com")
		i.AddWord("mofongo", "funnywords.com")
		i.AddWord("the", "commonwords.com")
		i.AddWord("rutabaga", "commonwords.com")
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		_ = i.Search("missing")
		_ = i.Search("missing")
		_ = i.Search("missing")
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		i.Clear()
	}()

	wg.Wait()
}
