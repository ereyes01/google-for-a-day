# google-for-a-day
A toy google knockoff...

This program provides a web interface that allows you to index web sites and search individual words in your index.

## Tests

To build and test the code, you need Go 1.12 or higher.

1. Clone this repository
2. Run the tests:
   - `go test -race`
   
## Running the Code

To run the code:

- `go run cmd/minigoogle/main.go &`

This will start a web server bound to port 9090. Run the command with the `-help` flag for instructions on customizing the bind address / port.

## Usage

To use the searching and indexing functionality, you must interact with the server using a web client like `curl`.

Below are some examples...

### Index a Site

To index a site:

```
curl -i -G localhost:9090/crawl --data-urlencode "site=https://fivethirtyeight.com"
```

This step might take a few moments.

### Search the Index

```
curl -q localhost:9090/search?word=NBA | jq ""
```

The results will be returned in JSON format, and sorted by number of occurrences. Above I'm using the `jq` command to format the JSON text into a more readable format, but using `jq` is optional.

### Clearing the Index

```
curl -i localhost:9090/clear
```

Everything in the index will be deleted once you clear it.
