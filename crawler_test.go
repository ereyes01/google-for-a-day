package search

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"text/template"

	"github.com/google/go-cmp/cmp"
)

type testRequest struct {
	path        string
	query       string
	mimeType    string
	payload     string
	shouldError bool
}

type testDef struct {
	msg              string
	sequence         []testRequest
	expectedIndex    map[string]wordMatches
	expectedSites    map[string]*site
	expectedNewWords int
	expectedNewSites int
}

func (tc *testDef) injectServerURL(baseURL string) {
	injectedIndex := make(map[string]wordMatches)
	for word, matches := range tc.expectedIndex {
		injectedMatches := make(wordMatches)

		for siteTpl, nHits := range matches {
			filledURL := replaceURL(siteTpl, baseURL)
			injectedMatches[filledURL] = nHits
		}
		injectedIndex[word] = injectedMatches
	}
	tc.expectedIndex = injectedIndex

	injectedSites := make(map[string]*site)
	for tplURL, site := range tc.expectedSites {
		filledURL := replaceURL(tplURL, baseURL)
		site.URL = filledURL
		injectedSites[filledURL] = site
	}
	tc.expectedSites = injectedSites

	var injectedSequence []testRequest
	for _, req := range tc.sequence {
		req.payload = replaceURL(req.payload, baseURL)
		injectedSequence = append(injectedSequence, req)
	}
	tc.sequence = injectedSequence
}

func replaceURL(tplText, URL string) string {
	inject := struct {
		BaseURL string
	}{
		BaseURL: URL,
	}

	tpl := template.Must(template.New("fill-base-url").Parse(tplText))
	result := &bytes.Buffer{}

	if err := tpl.Execute(result, inject); err != nil {
		panic(err)
	}

	return result.String()
}

type fixture struct {
	server         *httptest.Server
	crawler        *crawler
	def            *testDef
	callsRemaining int
	nCalls         int
	t              *testing.T
}

func (f *fixture) Handle(w http.ResponseWriter, r *http.Request) {
	f.t.Helper()

	seq := f.def.sequence[f.nCalls]
	f.nCalls++

	withSlash := seq.path
	if !strings.HasPrefix(seq.path, "/") {
		withSlash = "/" + seq.path
	}
	if r.URL.Path != withSlash {
		f.t.Fatalf("wrong path, got: %s expected: %s", r.URL.Path, withSlash)
	}
	if r.URL.RawQuery != seq.query {
		f.t.Fatalf("wrong query params, got %s expected: %s", r.URL.RawQuery, seq.query)
	}

	if seq.shouldError {
		w.WriteHeader(http.StatusInternalServerError)
	}

	w.Header().Set("Content-type", seq.mimeType)
	w.Write([]byte(seq.payload))
}

func setup(tc *testDef, t *testing.T) *fixture {
	if tc.expectedIndex == nil {
		tc.expectedIndex = make(map[string]wordMatches)
	}
	if tc.expectedSites == nil {
		tc.expectedSites = make(map[string]*site)
	}

	f := &fixture{
		def:            tc,
		t:              t,
		crawler:        newCrawler(newIndexerMap()),
		callsRemaining: len(tc.sequence),
	}

	f.server = httptest.NewServer(http.HandlerFunc(f.Handle))
	tc.injectServerURL(f.server.URL)
	return f
}

func tearDown(f *fixture, t *testing.T) {
	t.Helper()
	f.server.Close()
}

func TestCrawler(t *testing.T) {
	testCases := []testDef{
		{
			msg: "empty input, empty output",
		},
		{
			msg: "simple single html page",
			sequence: []testRequest{
				{
					mimeType: "text/html",
					payload:  "<html><head><title>simple</title></head><body>word1 word1</body></html>",
				},
			},
			expectedIndex: map[string]wordMatches{
				"word1":  wordMatches{"{{.BaseURL}}": 2},
				"simple": wordMatches{"{{.BaseURL}}": 1},
			},
			expectedSites: map[string]*site{
				"{{.BaseURL}}": &site{URL: "{{.BaseURL}}", Title: "simple"},
			},
			expectedNewWords: 2,
			expectedNewSites: 1,
		},
		{
			msg: "html page with path + query string",
			sequence: []testRequest{
				{
					path:     "a/path",
					query:    "var=val&var2=val2",
					mimeType: "text/html",
					payload:  "<html><head><title>simple</title></head><body>word1 word1</body></html>",
				},
			},
			expectedIndex: map[string]wordMatches{
				"word1":  wordMatches{"{{.BaseURL}}/a/path?var=val&var2=val2": 2},
				"simple": wordMatches{"{{.BaseURL}}/a/path?var=val&var2=val2": 1},
			},
			expectedSites: map[string]*site{
				"{{.BaseURL}}/a/path?var=val&var2=val2": &site{
					URL:   "{{.BaseURL}}/a/path?var=val&var2=val2",
					Title: "simple",
				},
			},
			expectedNewWords: 2,
			expectedNewSites: 1,
		},
		{
			msg: "simple single html page",
			sequence: []testRequest{
				{
					mimeType: "application/json",
					payload:  `{"not": "html"}`,
				},
			},
		},
		{
			msg: "do not index if error return code",
			sequence: []testRequest{
				{
					shouldError: true,
					mimeType:    "text/html",
					payload:     "<html><head><title>500</title></head><body>error</body></html>",
				},
			},
		},
		{
			msg: "html page with an absolute link",
			sequence: []testRequest{
				{
					mimeType: "text/html",
					payload:  `<html><head><title>simple</title></head><body><a href="{{.BaseURL}}/link">word1 word1</a></body></html>`,
				},
				{
					path:     "link",
					mimeType: "text/html",
					payload:  `<html><head><title>link</title></head><body>word1 word1</body></html>`,
				},
			},
			expectedIndex: map[string]wordMatches{
				"word1": wordMatches{
					"{{.BaseURL}}":      2,
					"{{.BaseURL}}/link": 2,
				},
				"simple": wordMatches{"{{.BaseURL}}": 1},
				"link":   wordMatches{"{{.BaseURL}}/link": 1},
			},
			expectedSites: map[string]*site{
				"{{.BaseURL}}":      &site{URL: "{{.BaseURL}}", Title: "simple"},
				"{{.BaseURL}}/link": &site{URL: "{{.BaseURL}}/link", Title: "link"},
			},
			expectedNewWords: 3,
			expectedNewSites: 2,
		},
		{
			msg: "html page with an relative link",
			sequence: []testRequest{
				{
					mimeType: "text/html",
					payload:  `<html><head><title>simple</title></head><body><a href="./link">word1 word1</a></body></html>`,
				},
				{
					path:     "link",
					mimeType: "text/html",
					payload:  `<html><head><title>link</title></head><body>word1 word1</body></html>`,
				},
			},
			expectedIndex: map[string]wordMatches{
				"word1": wordMatches{
					"{{.BaseURL}}":      2,
					"{{.BaseURL}}/link": 2,
				},
				"simple": wordMatches{"{{.BaseURL}}": 1},
				"link":   wordMatches{"{{.BaseURL}}/link": 1},
			},
			expectedSites: map[string]*site{
				"{{.BaseURL}}":      &site{URL: "{{.BaseURL}}", Title: "simple"},
				"{{.BaseURL}}/link": &site{URL: "{{.BaseURL}}/link", Title: "link"},
			},
			expectedNewWords: 3,
			expectedNewSites: 2,
		},
		{
			msg: "follow links only up to depth 3",
			sequence: []testRequest{
				{
					mimeType: "text/html",
					payload:  `<html><head><title>simple</title></head><body><a href="./link1">word1 word1</a></body></html>`,
				},
				{
					path:     "link1",
					mimeType: "text/html",
					payload:  `<html><head><title>link</title></head><body><a href="./link2">word1 word1</a></body></html>`,
				},
				{
					path:     "link2",
					mimeType: "text/html",
					payload:  `<html><head><title>link</title></head><body><a href="./link3">word1 word1</a></body></html>`,
				},
			},
			expectedIndex: map[string]wordMatches{
				"word1": wordMatches{
					"{{.BaseURL}}":       2,
					"{{.BaseURL}}/link1": 2,
					"{{.BaseURL}}/link2": 2,
				},
				"simple": wordMatches{"{{.BaseURL}}": 1},
				"link": wordMatches{
					"{{.BaseURL}}/link1": 1,
					"{{.BaseURL}}/link2": 1,
				},
			},
			expectedSites: map[string]*site{
				"{{.BaseURL}}":       &site{URL: "{{.BaseURL}}", Title: "simple"},
				"{{.BaseURL}}/link1": &site{URL: "{{.BaseURL}}/link1", Title: "link"},
				"{{.BaseURL}}/link2": &site{URL: "{{.BaseURL}}/link2", Title: "link"},
			},
			expectedNewWords: 3,
			expectedNewSites: 3,
		},
		{
			msg: "do not follow link cycles",
			sequence: []testRequest{
				{
					mimeType: "text/html",
					payload:  `<html><head><title>simple</title></head><body><a href="{{.BaseURL}}">word1 word1</a></body></html>`,
				},
			},
			expectedIndex: map[string]wordMatches{
				"word1":  wordMatches{"{{.BaseURL}}": 2},
				"simple": wordMatches{"{{.BaseURL}}": 1},
			},
			expectedSites: map[string]*site{
				"{{.BaseURL}}": &site{URL: "{{.BaseURL}}", Title: "simple"},
			},
			expectedNewWords: 2,
			expectedNewSites: 1,
		},
		{
			msg: "follow links in DFS order",
			sequence: []testRequest{
				{
					mimeType: "text/html",
					payload:  `<html><head><title>simple</title></head><body><a href="./link1">word1</a> <a href="./link2">word1</a></body></html>`,
				},
				{
					path:     "link1",
					mimeType: "text/html",
					payload:  `<html><head><title>link</title></head><body><a href="./link3">word1 word1</a></body></html>`,
				},
				{
					path:     "link3",
					mimeType: "text/html",
					payload:  `<html><head><title>link</title></head><body>word1 word1</body></html>`,
				},
				{
					path:     "link2",
					mimeType: "text/html",
					payload:  `<html><head><title>link</title></head><body><a href="./link4">word1 word1</a></body></html>`,
				},
				{
					path:     "link4",
					mimeType: "text/html",
					payload:  `<html><head><title>link</title></head><body><a href="./link5">word1 word1</a></body></html>`,
				},
			},
			expectedIndex: map[string]wordMatches{
				"word1": wordMatches{
					"{{.BaseURL}}":       2,
					"{{.BaseURL}}/link1": 2,
					"{{.BaseURL}}/link2": 2,
					"{{.BaseURL}}/link3": 2,
					"{{.BaseURL}}/link4": 2,
				},
				"simple": wordMatches{"{{.BaseURL}}": 1},
				"link": wordMatches{
					"{{.BaseURL}}/link1": 1,
					"{{.BaseURL}}/link2": 1,
					"{{.BaseURL}}/link3": 1,
					"{{.BaseURL}}/link4": 1,
				},
			},
			expectedSites: map[string]*site{
				"{{.BaseURL}}":       &site{URL: "{{.BaseURL}}", Title: "simple"},
				"{{.BaseURL}}/link1": &site{URL: "{{.BaseURL}}/link1", Title: "link"},
				"{{.BaseURL}}/link2": &site{URL: "{{.BaseURL}}/link2", Title: "link"},
				"{{.BaseURL}}/link3": &site{URL: "{{.BaseURL}}/link3", Title: "link"},
				"{{.BaseURL}}/link4": &site{URL: "{{.BaseURL}}/link4", Title: "link"},
			},
			expectedNewWords: 3,
			expectedNewSites: 5,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.msg, func(t *testing.T) {
			fixture := setup(&tc, t)
			defer tearDown(fixture, t)

			if len(tc.sequence) == 0 {
				return
			}

			// we'll only do the first one, the rest will have to happen by following links
			// in the returned html
			req := tc.sequence[0]
			URL := fixture.server.URL
			if req.path != "" {
				URL += "/" + req.path
			}
			if req.query != "" {
				URL += "?" + req.query
			}

			newWords, newSites, err := fixture.crawler.Crawl(URL)
			if err != nil {
				t.Fatal("unexpected error:", err)
			}

			if fixture.callsRemaining != fixture.nCalls {
				t.Fatalf("unexpected # of calls. got: %d expected: %d", fixture.nCalls, fixture.callsRemaining)
			}

			if newWords != tc.expectedNewWords {
				t.Fatalf("# new words, got: %d expected: %d", newWords, tc.expectedNewWords)
			}

			if newSites != tc.expectedNewSites {
				t.Fatalf("# new sites, got: %d expected: %d", newSites, tc.expectedNewSites)
			}

			if !cmp.Equal(fixture.def.expectedIndex, fixture.crawler.indexerMap.index) {
				t.Fatal("unexpected index:", cmp.Diff(fixture.def.expectedIndex, fixture.crawler.indexerMap.index))
			}
			if !cmp.Equal(fixture.def.expectedSites, fixture.crawler.indexerMap.sites) {
				t.Fatal("unexpected sites cache:", cmp.Diff(fixture.def.expectedSites, fixture.crawler.indexerMap.sites))
			}
		})
	}
}
