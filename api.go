package search

import (
	"encoding/json"
	"net/http"
)

// indexer interface allows us to inject a mock crawler / indexer
type indexer interface {
	Crawl(URL string) (int, int, error)
	Search(word string) []searchResult
	Clear()
}

// CrawlResponse tells how many words were indexed and how many sites were processed
// in a crawl operation.
type CrawlResponse struct {
	Words int `json:"words"`
	Sites int `json:"sites"`
}

// SearchHit contains one of the results you get in a search operation. A search will
// get you an array of these.
type SearchHit struct {
	URL         string `json:"url"`
	Title       string `json:"title"`
	Occurrences int    `json:"occurrences"`
}

// SearchAPI contains the web interfaces to the google-for-a-day search engine.
type SearchAPI struct {
	i indexer
}

// NewSearchAPI creates a new we api interface.
func NewSearchAPI() *SearchAPI {
	return &SearchAPI{
		i: newCrawler(newIndexerMap()),
	}
}

// CrawlHandler initiates a crawl operation. The query parameter "site" must
// contain the URL of a site you want to index.
func (s *SearchAPI) CrawlHandler(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	site := params.Get("site")

	words, sites, err := s.i.Crawl(site)
	if err != nil {
		http.Error(w, "crawl failed: "+err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-type", "application/json")
	resp := CrawlResponse{Words: words, Sites: sites}
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		http.Error(w, "failed to json encode response: "+err.Error(), http.StatusInternalServerError)
		return
	}
}

// SearchHandler initiates a search operation. The query parameter "word" must
// contain the word you want to search for.
func (s *SearchAPI) SearchHandler(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	word := params.Get("word")

	var resp []SearchHit
	results := s.i.Search(word)
	for _, hit := range results {
		resp = append(resp, SearchHit{
			URL:         hit.site.URL,
			Title:       hit.site.Title,
			Occurrences: hit.Occurrences,
		})
	}

	w.Header().Set("Content-type", "application/json")
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		http.Error(w, "failed to json encode response: "+err.Error(), http.StatusInternalServerError)
		return
	}
}

// ClearHandler initiates a clear operation. All indexed sites and words will
// be deleted.
func (s *SearchAPI) ClearHandler(w http.ResponseWriter, r *http.Request) {
	s.i.Clear()
}
