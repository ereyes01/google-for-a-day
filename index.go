package search

import (
	"sort"
	"sync"
)

// searchResult contains the number of times the search term occurs in a site's text
type searchResult struct {
	*site
	Occurrences int `json:"occurrences"`
}

// site represents a single site / search result
type site struct {
	// URL that gets you to the site
	URL string `json:"url"`

	// Title of the site, taken from the <head><title> tag
	Title string `json:"title"`
}

// wordMatches maps url -> # occurrences of a word
type wordMatches map[string]int

// indexerMap maintains an index of search terms -> websites. The index is implemented with 3 layers of maps:
// word -> URL -> #occurrences. Search results are sorted at search time. This leads to O(1) index time for
// each word, and O(NlogN) search time. The indexerMap is thread-safe, and can therefore be safely called
// from a web server handler.
type indexerMap struct {
	sync.RWMutex

	// index maps an indexed word -> a set of search results
	index map[string]wordMatches

	// sites maps URL -> site info
	sites map[string]*site
}

// newIndexerMap creates and initializes a site search indexerMap.
func newIndexerMap() *indexerMap {
	return &indexerMap{
		index: make(map[string]wordMatches),
		sites: make(map[string]*site),
	}
}

// Clear empties the index
func (i *indexerMap) Clear() {
	i.Lock()
	defer i.Unlock()

	i.index = make(map[string]wordMatches)
	i.sites = make(map[string]*site)
}

// AddWord adds a word for the given site into the index.
func (i *indexerMap) AddWord(word, URL string) {
	i.Lock()
	defer i.Unlock()

	if m, present := i.index[word]; present {
		m[URL]++
	} else {
		i.index[word] = wordMatches{URL: 1}
	}
}

// AddSite associates website URL with title.
func (i *indexerMap) AddSite(URL, title string) {
	i.Lock()
	defer i.Unlock()

	i.sites[URL] = &site{URL: URL, Title: title}
}

func (i *indexerMap) IsSiteIndexed(URL string) bool {
	i.RLock()
	defer i.RUnlock()

	_, present := i.sites[URL]
	return present
}

// Search for given word in the index, return hits / sites sorted by # occurrences of word
func (i *indexerMap) Search(word string) []searchResult {
	var done bool

	i.RLock()
	defer func() {
		if !done {
			i.RUnlock()
		}
	}()

	hits, present := i.index[word]
	if !present {
		return nil
	}

	var results []searchResult

	for url, n := range hits {
		site, present := i.sites[url]
		if !present {
			continue
		}

		results = append(results, searchResult{site: site, Occurrences: n})
	}

	// let's release the lock before the potentially expensive sorting
	done = true
	i.RUnlock()

	sort.Slice(results, func(i, j int) bool { return results[i].Occurrences > results[j].Occurrences })
	return results
}
