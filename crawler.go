package search

import (
	"io"
	"log"
	"net/http"
	"net/url"
	"strings"

	"golang.org/x/net/html"
)

type crawler struct {
	*indexerMap
}

func newCrawler(indexer *indexerMap) *crawler {
	return &crawler{
		indexerMap: indexer,
	}
}

const maxFollowDepth = 3

// Crawl will GET the given URL and index words it finds in the html body. It will also recursively
// GET and index any pages linked in the html up to a crawl depth of 3. HTTP responses with non-200
// status codes are not indexed.
func (c *crawler) Crawl(URL string) (int, int, error) {
	newWords := make(map[string]struct{})
	newSites := make(map[string]struct{})

	err := c.crawlSite(URL, maxFollowDepth, newWords, newSites)
	return len(newWords), len(newSites), err
}

func (c *crawler) crawlSite(URL string, depthLeft int, newWords, newSites map[string]struct{}) error {
	if depthLeft <= 0 {
		return nil
	}

	// we disallow re-indexing of the same site more than once (avoids link cycles)
	if c.IsSiteIndexed(URL) {
		log.Println("not re-indexing site:", URL)
		return nil
	}

	var closed bool

	resp, err := http.Get(URL)
	if err != nil {
		return err
	}
	defer func() {
		if !closed {
			resp.Body.Close()
		}
	}()

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return nil
	}
	if !strings.Contains(resp.Header.Get("Content-type"), "html") {
		return nil
	}

	newSites[URL] = struct{}{}
	links := c.parse(URL, resp.Body, newWords)

	resp.Body.Close()
	closed = true

	baseURL, err := url.Parse(URL)
	if err != nil {
		return err
	}

	for _, link := range links {
		linkURL, err := url.Parse(link)
		if err != nil {
			log.Println("bad link:", err)
			continue
		}

		absLink := baseURL.ResolveReference(linkURL).String()
		if err := c.crawlSite(absLink, depthLeft-1, newWords, newSites); err != nil {
			return err
		}
	}

	return nil
}

func getHref(token html.Token) (string, bool) {
	for _, attr := range token.Attr {
		if attr.Key == "href" {
			return attr.Val, true
		}
	}

	return "", false
}

func (c *crawler) parse(URL string, body io.Reader, newWords map[string]struct{}) []string {
	var (
		inTitle bool
		links   []string
	)
	tokens := html.NewTokenizer(body)

	for {
		node := tokens.Next()
		token := tokens.Token()

		switch {
		case node == html.ErrorToken:
			return links
		case node == html.StartTagToken:
			if token.Data == "title" {
				inTitle = true
			} else if token.Data == "a" {
				href, present := getHref(token)
				if present {
					links = append(links, href)
				}
			}
		case node == html.EndTagToken:
			if token.Data == "title" {
				inTitle = false
			}
		case node == html.TextToken:
			if inTitle {
				c.AddSite(URL, token.Data)
			}
			for _, word := range strings.Fields(token.Data) {
				newWords[word] = struct{}{}
				c.AddWord(word, URL)
			}
		}
	}
}
